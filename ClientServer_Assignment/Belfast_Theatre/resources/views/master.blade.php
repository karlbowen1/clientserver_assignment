<!doctype html>

<html lang="en">
    
   <head>
    
       <meta charset = "UTF-8"> 
       
       <title>Belfast Theatre Company</title>
       
       <link rel="stylesheet" href="/css/style.css"/>
   
   </head>
        
	<body>
            
	@yield('content')
        
        @yield('footer')
        
        </body>
</html>