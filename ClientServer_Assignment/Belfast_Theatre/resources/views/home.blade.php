<html>
    <head>
        
        <title> Belfast Theatre Home </title>
        <link rel = "stylesheet" type = "text/css" href = "/css/style.css" />
    </head>
    
    <body>
         <div id = "container">
        
            <div id = "title">
               <h1> Belfast Theatre Company</h1>                 
            </div> 
            
            <div id = "nav">
               <div id = "nav_wrapper"> 
                  <ul>  
                     <li><a href="#">Home</a> </li> <li>     
                     <a href="#">What's On </a>
                        <ul>
                           <li><a href="#">Mamma Mia</a></li>
                           <li><a href="#">The King's Speech</a></li>
                           <li><a href="#">The Producers</a></li>
                        </ul>
                     </li>
                     <li> <a href="#">About Us</a> </li> 
                     <li> <a href="#">Contact Us</a> </li> 
                  </ul>
               </div>
            </div> 
                     
            <div id = "content">  
               <div id = "main"> 
                  <div id = "slider"> <!-- Slider container -->
                     <div id="mask">  <!-- Mask -->
                     
                     <ul>
                     
                     <li id = "first" class = "firstanimation"> 
                     <a href="#"> <img src="images/theatre-1.jpg" alt="theatre"/> </a>
                     <div class = "tooltip"> <h1>Welcome to the show!</h1> </div> 
                     </li>
                     
                     <li id = "second" class = "secondanimation">
                     <a href="#"> <img src="images/mamma_mia.jpg" alt="mamma mia"/> </a>
                     <div class="tooltip"> <h1>Mamma Mia!</h1> </div>
                     </li>
                     
                     <li id = "third" class = "thirdanimation">
                     <a href="#"> <img src="images/kings_speech.jpg" alt="kings speech"/> </a>
                     <div class="tooltip"> <h1>The King's Speech</h1> </div>
                     </li>
                     
                     <li id = "fourth" class = "fourthanimation">
                     <a href="#"> <img src="images/producers.jpg" alt="the producers"/> </a>
                     <div class="tooltip"> <h1>The Producers</h1> </div>
                     </li>
                     
                     <li id = "fifth" class = "fifthanimation">
                     <a href="#"> <img src="images/kings_speech.jpg" alt="kings speech"/> </a>
                     <div class="tooltip"> <h1>The Kings Speech</h1> </div>
                     </li>
                     
                     </ul>
               
                     </div>  <!-- End of Mask -->
                     <div class = "progress-bar" > </div>
                  </div> <!-- End of Slider -->
               </div> 
             
            <h3> Welcome to the Belfast Theatre Company </h3> 
            <p> Stuff will go here. </p> 
        
         </div> 
            
        <div id = "footer">
            Copyright &copy; 2015 Belfast Theatre Company.
        </div>
        </div>
    </div>
    </body>
        
        
    
</html>